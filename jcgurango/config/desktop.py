from frappe import _

def get_data():
	return [
		{
			"module_name": "JC Gurango",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("JC Gurango")
		}
	]
